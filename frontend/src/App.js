import './App.css';
import BookEffect from './components/BookEffect';
import PageAdd from './components/PageAdd';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import './styles/book.css';
import './styles/edit-page.css';
import EditPage from './components/EditPage';
import AllPages from './components/AllPages';

function App() {
  return (
    <div className="middle">
      <Router>
        <Switch>
          <Route path="/" exact component={BookEffect} />
          <Route path="/add" exact component={PageAdd} />
          <Route path="/edit" exact component={EditPage} />
          <Route path="/all" exact component={AllPages} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
