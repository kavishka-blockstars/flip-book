import FileBase64 from 'react-file-base64';
import { useState } from 'react';
import axios from 'axios';

export default function PageAdd() {
    const [image, setImage] = useState({});

    function sendImage(){
        axios.post('http://localhost:8090/images/',{'images':image});
        window.location.href = '/';
    }

    return (
        <div className="container">
            <h1 style={{ color: "white" }}>Adding form</h1>
            <div style={{backgroundColor:"white", padding:"5%", borderRadius:"12px"}}>
            <FileBase64
            type="file"
            multiple={false}
            onDone={ ({base64})=>setImage(base64)}
                 />
            </div>
                 <button onClick={sendImage} className="btn" style={{marginTop:"10%", backgroundColor:"#FA334E", color:"white"}}>Add a page</button>
        </div>
    );
}