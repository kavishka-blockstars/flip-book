import HTMLFlipBook from 'react-pageflip';
import { useEffect, useState } from 'react';
import axios from 'axios';
export default function BookEffect() {

    const [imageData, setImageData] = useState([]);

    useEffect(()=>{
        axios.get('http://localhost:8090/images/').then((images)=>{
            setImageData(images.data);
        });
    },[]);

    return (
        <div className="container">
            <div>
                <div className="d-flex justify-content-center align-items-center" style={{display:"flex"}}>
                    <div style={{flex:"5"}}><h1 style={{color:"white", margin:"5% 0", textAlign:"center"}}>Read your book here.</h1></div>
                    <div style={{flex:"1"}}><a href="/all" className="btn" style={{margin:"5%", backgroundColor:"#FA334E", color:"white"}}>Edit page</a></div>
                    <div style={{flex:"1"}}><a href="/add" className="btn" style={{margin:"5%", backgroundColor:"#FA334E", color:"white"}}>Add a page</a></div>
                </div>
                <div className="d-flex justify-content-center align-items-center">
                <HTMLFlipBook 
                width={350} 
                height={600}
            >
                <div></div>
                {imageData.map((images)=>{
                    return(
                        <div key={images._id} className="page">
                            <img className="image" alt="image1" src={images.image}/>
                            <h4 style={{color:"red"}}> Kavishka </h4>
                        </div>
                    );
                })}
                
            </HTMLFlipBook>
                </div>
            </div>
        </div>);
}