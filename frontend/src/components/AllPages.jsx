import { useEffect, useState } from "react";
import axios from 'axios';

export default function AllPages(){

    const [pages, setPages] = useState([]);

    useEffect(()=>{
        axios.get('http://localhost:8090/cards/').then((pages)=>{
            //setPages
            setPages(pages.data);
        });
    })

    return(
        <div className="container">
            <div style={{display:"grid", gridTemplateColumns: "400px 400px 400px"}}>
            {pages.map((page)=>{
                return(
                    <div style={{width:"150px", height:"200px", background:"url({pa})", margin:"5%", border:"1px solid #fff", borderRadius:"9px"}} />
                );
            })}
            </div>
        </div>
    );
}