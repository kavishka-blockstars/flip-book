import { useState } from "react";
import Draggable from 'react-draggable';
import axios from 'axios';
import FileBase64 from 'react-file-base64';

export default function EditPage() {

    const [editable, setEditable] = useState(false);
    const [value, setValue] = useState({
        title: '',
        subTitle: '',
        description: '',
        image: ''
    });

    /*useEffect(()=>{
        axios.post('http://localhost:8090/cards/',value);
    },[]);*/

    function clickOnItem() {
        if (editable === false) {
            setEditable(true);
        } else {
            setEditable(false);
        }
    }

    function Save() {
        axios.post('http://localhost:8090/cards/', value);
    }

    /*function valueChange(e){
        const {textContent} = e.currentTarget;
        const {name} = e.target;
        setValue(prevValue=> ({
            ...prevValue,
            [name] : textContent
            })
        );

        console.log(value);
    }*/

    return (
        <div className="container edit-page">
            <div className="d-flex justify-content-center">
                <h1>Editable Page</h1>
            </div>
            <div className="d-flex justify-content-center">
                <div className="image">
                    <Draggable>
                        <h3 onDoubleClick={clickOnItem} contentEditable={editable} onInput={e => setValue({ ...value, title: e.currentTarget.textContent })}>{/*value==null? "Title": value*/}Title</h3>
                    </Draggable>
                    <Draggable>
                        <h5 onDoubleClick={clickOnItem} contentEditable={editable} onInput={e => setValue({ ...value, subTitle: e.currentTarget.textContent })}>Sub-Title</h5>
                    </Draggable>
                    <Draggable
                        axis="both"
                        bounds="body"
                        grid={[25, 25]}>
                        <p onDoubleClick={clickOnItem} contentEditable={editable} onInput={e => setValue({ ...value, description: e.currentTarget.textContent })}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </Draggable>
                </div>
            </div>
            <div style={{display:"flex"}}>
            <div style={{flex:"3"}}>
                <FileBase64
                    type="file"
                    multiple={false}
                    onDone={({ base64 }) => setValue({ ...value, image: base64 })}
                />
            </div>
            <div style={{flex:"1"}}><div className="btn" style={{ margin: "5%", backgroundColor: "#FA334E", color: "white" }} onClick={Save}>Save</div></div>
            </div>
        </div>
    );
}