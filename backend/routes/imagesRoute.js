const Image = require('../models/imagesModel');
const router = require('express').Router();

router.route('/').post((req,res)=>{
    const newImage = new Image({"image":req.body.images});

    newImage.save().then(()=>{
        res.send('Saved successfully');
    }).catch((err)=>{
        console.log(err);
    });
});

router.route('/').get((req,res)=>{
    Image.find().then((image)=>{
        res.json(image);
    }).catch((err)=>{
        console.log(err);
    })
});

module.exports = router;