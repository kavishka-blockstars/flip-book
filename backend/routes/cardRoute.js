const Card = require('../models/cardModel');
const router = require('express').Router();

router.route('/').post((req,res)=>{
    const newCard = new Card(req.body);

    newCard.save().then(()=>{
        res.send('Success');
    }).catch((err)=>{
        console.log(err);
    });
});

router.route('/').get((req,res)=>{
    Card.find().then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
});

router.route('/:id').put((req,res)=>{
    const id = req.params.id;
    const data = req.body;
    Card.findByIdAndUpdate(id, {$set: data}).then(()=>{
        res.send('Updated');
    }).catch((err)=>{
        console.log(err);
    });
})

module.exports = router;