const express = require("express");
const mongoose =require("mongoose");
const bodyParser =require("body-parser");
const cors=require("cors");
const Images = require('./routes/imagesRoute');
const Cards = require('./routes/cardRoute');
const app = express();

app.use(cors());

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use('/images',Images);
app.use('/cards',Cards);

mongoose.connect('mongodb://localhost:27017/images');

app.listen(8090,()=>{
    console.log(`Server is up and running on port 8090`);
});