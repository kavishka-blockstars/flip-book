const mongoose = require('mongoose');

const CardSchema = new mongoose.Schema({
    title: String,
    subTitle: String,
    description: String,
    image: String
});

const Card = new mongoose.model('Card', CardSchema);

module.exports = Card;